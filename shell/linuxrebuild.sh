cd ../../
cd rat-common
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j20
make test
sudo make install
cd ../../

cd materials-cpp
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j20
make test
sudo make install
cd ../../

# cd materials-data
# git checkout dev
# git pull origin dev
# sudo rm -r build
# # sudo rm -r release
# mkdir build
# cd build
# cmake -DCMAKE_BUILD_TYPE=Release ..
# make -j20
# make test
# sudo make install
# cd ../../

cd distmesh-cpp
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j20
make test
sudo make install
cd ../../

cd rat-mlfmm
git checkout dev
git pull origin dev
# sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j20
make test
sudo make install
cd ../../

cd rat-models
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j20
make test
sudo make install
cd ../../

cd raccoon2
git checkout dev
git pull origin dev
sudo rm -r build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j20
make test
sudo make install
cd ../../


cd rat-gui
git checkout dev
git pull origin dev
cd corrade
git checkout master
git pull origin master
cd ../magnum
git checkout master
git pull origin master
cd ../magnum-plugins
git checkout master
git pull origin master
cd ../magnum-integration
git checkout master
git pull origin master
cd ../imgui
git checkout docking
git pull origin docking
cd ../implot
git checkout master
git pull origin master
cd ../filedialog
git checkout c8ccc5f
# git pull origin c8ccc5f
cd ../meshoptimizer
git checkout master
git pull origin master
cd ..
sudo rm -r build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j20
make test
sudo make install
cd ../../
cd rat-documentation/shell


